using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
public class TrackMultiple : MimiBehaviour 
{
    //parameters
    [SerializeField] private List<Transform> toTrack = new List<Transform>(1);
    [SerializeField] private float m_fMoveSpeed = 5.0f;

    //component references
    private Camera cam;

    protected override void Awake()
    {
        base.Awake();
    }

    void Start () 
    {
        //get component references
        cam = GetComponent<Camera>();
    }

    //Move Camera in FixedUpdate as our tracked object only moves in after a FixedUpdate
	void FixedUpdate () 
    {

        Vector3 v3Center = Vector3.zero;//new target position = centroid of polygon formed by positions of all tracked objects

        foreach (Transform t in toTrack)
        {
            if (t == null) continue;

            v3Center += t.position;
        }

        v3Center /= toTrack.Count;//center = avg position

        //interpolate position
        m_transThis.position = Vector3.Lerp(m_transThis.position, new Vector3(v3Center.x, m_transThis.position.y, v3Center.z), Time.smoothDeltaTime * m_fMoveSpeed);
    }
}
