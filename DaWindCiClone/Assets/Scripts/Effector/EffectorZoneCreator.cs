using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectorZoneCreator : MimiBehaviour
{
    [SerializeField] private KeyCode m_DragKeyCode = KeyCode.Mouse0;
    
    [SerializeField] private EffectorZone m_goEffectorZonePrefab;
    
    [SerializeField] private float m_fMinimumForce;
    [SerializeField] private float m_fMaximumForce;
    [SerializeField] private float m_fPlayerWeight;
    [SerializeField] private float m_fMinimumDistance;
    [SerializeField] private float m_fWidth;
    [SerializeField] private float m_fLifetime;

    private Plane m_ClickPlane = new Plane(Vector3.up, Vector3.zero);
    
    private Vector3 m_v3PositionLastFrame;
    private Vector3 m_v3CurrentStartPosition;
    
    protected override void Awake()
    {
        base.Awake();
    }

    protected void Start()
    {
        //CreateEffectorZone(new Vector3(0, 0, 0), new Vector3(0, 0, 10f), 3, m_fWidth);
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            StartDrag();
        }
        else if(Input.GetKey(KeyCode.Mouse0))
        {
            OnDrag();
            m_v3PositionLastFrame = GetMousePositionOnXZPlane();
        }
        else if(Input.GetKeyUp(KeyCode.Mouse0))
        {
            EndDrag();
        }
    }

    //Uses a camera ray to determine where the player has clicked on the Y = 0 plane
    private Vector3 GetMousePositionOnXZPlane()
    {
        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        float fHitDistance;
        m_ClickPlane.Raycast(cameraRay, out fHitDistance);

        return cameraRay.GetPoint(fHitDistance);
    }

    private void StartDrag()
    {
        m_v3CurrentStartPosition = GetMousePositionOnXZPlane();
    }

    private void OnDrag()
    {
        //Determine current distance from last start
        Vector3 v3CurrentPosition = GetMousePositionOnXZPlane();
        float fDistance = Vector3.SqrMagnitude(v3CurrentPosition - m_v3CurrentStartPosition);

        //If current distance is higher than the minimum distance we create a zone and move the start position
        if (fDistance >= m_fMinimumDistance * m_fMinimumDistance)
        {
            //Determine the current speed
            float fcurrentSpeed = (v3CurrentPosition - m_v3PositionLastFrame).magnitude * Time.deltaTime;
            float fForceToApply = Mathf.Clamp(m_fPlayerWeight * fcurrentSpeed, m_fMinimumForce, m_fMaximumForce);
            
            
            CreateEffectorZone(m_v3CurrentStartPosition, v3CurrentPosition, fForceToApply, m_fWidth, m_fLifetime);
            m_v3CurrentStartPosition = v3CurrentPosition;
        }
    }

    private void EndDrag()
    {
        
    }

    private EffectorZone CreateEffectorZone(Vector3 _v3Start, Vector3 _v3End, float _fForce, float _fWidth, float _fLifetime)
    {
        EffectorZone NewZone =
            GameObject.Instantiate<EffectorZone>(m_goEffectorZonePrefab, Vector3.zero, Quaternion.identity);
        
        NewZone.SetForce(_v3Start, _v3End, _fForce, _fWidth, _fLifetime);

        return NewZone;
    }
}
