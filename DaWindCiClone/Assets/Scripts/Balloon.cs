using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Balloon : MimiBehaviour, IEffectable
{
    private bool m_bEffectedThisFrame = false;
    private Rigidbody m_rigidThis;
    
    protected override void Awake()
    {
        base.Awake();
        m_rigidThis = GetComponent<Rigidbody>();
    }

    public void Effect(Vector3 force)
    {
        if(HasBeenEffected())
            return;
        
        m_rigidThis.AddForce(force, ForceMode.Force);
    }

    private void LateUpdate()
    {
        m_bEffectedThisFrame = false;
    }

    public bool HasBeenEffected()
    {
        return m_bEffectedThisFrame;
    }
}
