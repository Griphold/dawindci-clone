using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEffectable
{
    void Effect(Vector3 force);

    bool HasBeenEffected();
}
