using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EffectorZone : MimiBehaviour
{
    private Vector3 m_v3Force;
    
    protected override void Awake()
    {
        base.Awake();
    }

    protected void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<Balloon>().Effect(m_v3Force);
        }
    }

    /// <summary>
    /// Create a BoxCollider from start to end with the given force.
    /// </summary>
    /// <param name="_v3StartPosition"></param>
    /// <param name="_v3EndPosition"></param>
    /// <param name="_fForce"></param>
    public void SetForce(Vector3 _v3StartPosition, Vector3 _v3EndPosition, float _fForce, float _fWidth, float _fTimeout)
    {
        Vector3 v3Direction = _v3EndPosition - _v3StartPosition;
        m_v3Force = v3Direction.normalized * _fForce;

        m_transThis.position = (_v3StartPosition + _v3EndPosition) / 2f; //position box in the middle
        m_transThis.rotation = Quaternion.LookRotation(m_v3Force, Vector3.up); //rotate along force axis
        m_transThis.localScale = new Vector3(_fWidth, 1, v3Direction.magnitude); //scale Box along local Z axis
        
        //Start death timer
        Destroy(this.gameObject, _fTimeout);
    }
}
